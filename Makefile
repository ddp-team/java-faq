# Makefile for a manual in the Debian Documentation Project manuals.sgml
# tree.

# The directory in which this makefile resides must also contain a file
# called <directoryname>.sgml, which is the top-level file for the manual
# in this directory.

# What is the current manual's name
#MANUAL :=	$(shell basename $(shell pwd))
MANUAL :=	debian-java-faq
# Where are we publishing to?
#  (this can be overriden by a higher level makefile)
PUBLISHDIR := /org/www.debian.org/www/doc/manuals
#PUBLISHDIR := ~/public_html/manuals.html/

# Languages:
# languages supported by SGML files (always: en)
LANGS_EN        := en it
# languages supported by translation PO files
LANGS_PO        := fr

# List of languages built for "publish" target for DDP
LANGS           := $(LANGS_EN) $(LANGS_PO)
# translation contents directory such as PO file
PO4A            := po4a
# The translation threshold percent to accept a translation. Should be 80 to consider 
# the translation almost completed.
THRESHOLD       := 50

# Formats to be generated
FORMATS = html txt
# Note: PDF not included because it is not working properly with UTF-8 files yet:
# FORMATS = html txt pdf
# Files generated when generating above formats
EXTRANEOUS_FORMATS = tpt sasp tex toc log out aux

#
# List of html stamp files to be built
HTMLS := $(foreach lang,$(LANGS),$(MANUAL).$(lang).html.stamp)
# List of txt to be built
TXTS := $(foreach lang,$(LANGS),$(MANUAL).$(lang).txt)

# List of ps to be built
PSS := $(foreach lang,$(LANGS),$(MANUAL).$(lang).ps)

# List of pdf to be built
PDFS := $(foreach lang,$(LANGS),$(MANUAL).$(lang).pdf)

##############################################################################
# PO update
##############################################################################
# Usually, POMODE =  update|static
POMODE  := static


##############################################################################
# Basic phony targets for building
##############################################################################

build: build-indep

build-indep: all

##############################################################################
# Utility phony targets
##############################################################################

# ensure our SGML is valid
#   (add this to $(MANUAL).html rule to prevent building if not)
validate:
		nsgmls -gues $(MANUAL).sgml


# =================================================================== #
#                 Build target part: Customize                        #
# =================================================================== #
# If some languages have problems building, filter-out in here.

# What do we want by default?
all: html txt 
# This are disabled currently: ps pdf
html: $(HTMLS)
text txt:  $(TXTS)
ps:   $(PSS)
pdf:  $(PDFS)

publish: publish-html publish-txt
# This are disabled currently: publish-ps publish-pdf

# This target installs the generated HTML in the published directory.
#publish:	$(MANUAL).html/index.html
# 		fail if there is no PUBLISHDIR
#		[ -d $(PUBLISHDIR) ] || exit 1
#		rm -f $(PUBLISHDIR)/$(MANUAL)/*.html
#		install -d -m 755 $(PUBLISHDIR)/$(MANUAL)
#		install -m 644 --preserve-timestamps $(MANUAL).html/*.html \
#			$(PUBLISHDIR)/$(MANUAL)/



# =================================================================== #
#                 Build rule part: Routine                            #
# =================================================================== #
# If some languages have problems building, filter-out in here.

# PO file generation
po:
	touch $(MANUAL).sgml
	$(MAKE) $(foreach lng, $(LANGS_PO), $(PO4A)/$(MANUAL).$(lng).po) POMODE=update
pot:    $(PO4A)/po/$(MANUAL).pot

# Generate base localized SGML files for all non-"en"
# This does not depend on $(PO4A)/po/$(MANUAL).%.po intentionally
# to avoid automatic update of $(PO4A)/po/$(MANUAL).%.po.
$(MANUAL).%.sgml: $(MANUAL).sgml $(PO4A)/po/$(MANUAL).%.po
	po4a-translate --format sgml --keep $(THRESHOLD) --master-charset UTF-8 \
		--master $< --po $(PO4A)/po/$(MANUAL).$*.po --localized $@ 

# define $(locale) for the following targets
$(MANUAL).%.html.stamp $(MANUAL).%.txt $(MANUAL).%.ps $(MANUAL).%.pdf: \
  locale=$(subst pt-br,pt_BR,\
         $(subst zh-cn,zh_CN,\
         $(subst fr,fr.UTF-8,\
         $*)))

# =================================================================== #
#                 SGML PO source file preparations                    #
# =================================================================== #

ifeq ($(POMODE),update)
# Always update localized PO files while making PO template(pot) as needed
# .PHONY as follows does not work nicely with %, thus FORCE is used here.
#.PHONY: $(foreach lng, $(LANGS_PO), $(PO4A)/$(MANUAL).$(lng).po)
$(PO4A)/po/$(MANUAL).%.po: FORCE
	$(MAKE) $(PO4A)/po/$(MANUAL).pot
	msgmerge --update --previous $@ $(PO4A)/$(MANUAL).pot
else
$(PO4A)/$(MANUAL).%.po: FORCE
	: # do nothing
endif

# Generate PO template(pot) file from English DocBook XML file
# make sure no location line for easier merge.
$(PO4A)/po/$(MANUAL).pot: $(MANUAL).sgml
	po4a-gettextize --format sgml --master-charset UTF-8 --master $< |\
		msgcat --no-location - > $@

# phony targets for PO maintainances
tidypo:
	for f in $(LANGS_PO); do msgcat --no-location $(PO4A)/po/$(MANUAL).$$f.po | sponge $(PO4A)/po/$(MANUAL).$$f.po ; done
	msgcat --no-location $(PO4A)/po/$(MANUAL).pot | sponge $(PO4A)/po/$(MANUAL).pot

update-po: 
	PERL_PERTURB_KEYS=0 PERL_HASH_SEED=0 po4a --force --previous --no-translations --rm-backups po4a/po4a.cfg

po4a-translate: 
	PERL_PERTURB_KEYS=0 PERL_HASH_SEED=0 po4a --keep 0 --previous --rm-backups po4a/po4a.cfg # --localized-charset UTF-8


# English version
$(MANUAL).en.txt: $(MANUAL).txt
	mv debian-java-faq.txt debian-java-faq.en.txt
$(MANUAL).en.ps: $(MANUAL).ps
	mv debian-java-faq.ps debian-java-faq.en.ps
$(MANUAL).en.pdf: $(MANUAL).pdf
	mv debian-java-faq.pdf debian-java-faq.en.pdf

# TXT

$(MANUAL).txt:	$(MANUAL).sgml
	debiandoc2text $<
$(MANUAL).%.txt: $(MANUAL).%.sgml 
	debiandoc2text -l $(locale) $<

# PS

$(MANUAL).ps:	$(MANUAL).sgml
	debiandoc2latexps  $<
$(MANUAL).%.ps: $(MANUAL).%.sgml
	debiandoc2latexps -l $(locale) $<

# PDF
$(MANUAL).pdf:	$(MANUAL).sgml
	debiandoc2pdf $<
$(MANUAL).%.pdf: $(MANUAL).%.sgml
	debiandoc2pdf -l $(locale) $<

# DVI
$(MANUAL).dvi: $(MANUAL).sgml
	debiandoc2dvi $<
$(MANUAL).%.dvi: $(MANUAL).%.sgml
	debiandoc2dvi $<

# HTML
$(MANUAL).en.html.stamp: $(MANUAL).sgml
		debiandoc2html -C  $<
		if [ ! -e debian-java-faq.en.html ] ; then \
			mv debian-java-faq.html debian-java-faq.en.html ; else \
			mv debian-java-faq.html/* debian-java-faq.en.html/ ; \
		fi
		touch $(MANUAL).en.html.stamp
$(MANUAL).%.html.stamp:	$(MANUAL).%.sgml
		debiandoc2html -l $(locale) -C  $<
# since $(MANUAL).%.html/index.%.html cannot be a target file
		@[ ! -e $(MANUAL).$*.html ] || for file in `ls $(MANUAL).$*.html/*` ; do \
        newfile=`echo $$file|\
            sed 's/$(shell echo $*|\
                sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZ_/abcdefghijklmnopqrstuvwxyz-/'\
                )\.html/$*\.html/'`; \
        if [ $$file != $$newfile ] ; then \
                mv $$file $$newfile; \
                echo "Rename $$file --> $$newfile"; \
        fi \
        done
		touch $(MANUAL).$*.html.stamp


# =================================================================== #
#                 Build rule part: Web publish                        #
# =================================================================== #

publish-html: html
	test -d $(PUBLISHDIR)/$(MANUAL) \
		|| install -d -m 755 $(PUBLISHDIR)/$(MANUAL)
	rm -f $(PUBLISHDIR)/$(MANUAL)/*.html
# install all html
	$(foreach lang,$(LANGS),\
	install -p -m 644 $(MANUAL).$(lang).html/*.html \
	$(PUBLISHDIR)/$(MANUAL)/ ;\
        )
publish-txt:  txt
		test -d $(PUBLISHDIR)/$(MANUAL) \
			|| install -d -m 755 $(PUBLISHDIR)/$(MANUAL)
		rm -f $(PUBLISHDIR)/$(MANUAL)/*.txt
		# install all txt
		@$(foreach lang,$(LANGS),\
		install -p -m 644 $(MANUAL).$(lang).txt \
			$(PUBLISHDIR)/$(MANUAL)/ ;\
        )

publish-ps:  ps
		test -d $(PUBLISHDIR)/$(MANUAL) \
		|| install -d -m 755 $(PUBLISHDIR)/$(MANUAL)
		rm -f $(PUBLISHDIR)/$(MANUAL)/*.ps
		# install all ps
		@$(foreach lang,$(LANGS),\
			install -p -m 644 $(MANUAL).$(lang).ps \
			$(PUBLISHDIR)/$(MANUAL)/ ;\
		)

publish-pdf:  pdf
		test -d $(PUBLISHDIR)/$(MANUAL) \
			|| install -d -m 755 $(PUBLISHDIR)/$(MANUAL)
		rm -f $(PUBLISHDIR)/$(MANUAL)/*.pdf
		# install all pdf1
		@$(foreach lang,$(LANGS),\
			install -p -m 644 $(MANUAL).$(lang).pdf \
			$(PUBLISHDIR)/$(MANUAL)/ ;\
		)


# =================================================================== #
#                 Build rule part: Cleanup                            #
# =================================================================== #

clean:
		rm -rf $(foreach fmt, $(FORMATS), $(MANUAL).$(fmt)) \
			$(foreach fmt, $(EXTRANEOUS_FORMATS), $(MANUAL).$(fmt))  \
			$(foreach lng, $(LANGS), $(foreach fmt, $(FORMATS), $(MANUAL).$(lng).$(fmt))) \
			$(foreach lng, $(LANGS), $(foreach fmt, $(EXTRANEOUS_FORMATS), $(MANUAL).$(lng).$(fmt))) \
		rm -f *.stamp

distclean:	clean
	rm -f $(foreach lng, $(LANGS_PO), $(MANUAL).$(lng).sgml)

.PHONY: all html text txt ps pdf \
		build build-indep \
        publish publish-html publish-tst publish-ps publish-pdf \
        tidypo update-po po4a-translate po pot validate
